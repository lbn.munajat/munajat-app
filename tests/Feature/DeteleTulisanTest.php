<?php

namespace Tests\Feature;

use App\Filament\Resources\TulisanResource;
use App\Models\tulisan;
use Filament\Pages\Actions\DeleteAction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class DeteleTulisanTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_render_page()
    {
        $tulisan = tulisan::factory()->create();
        

        Livewire::test(TulisanResource\Pages\EditTulisan::class,
        [
                    'record' => $tulisan->getKey(),
        ])
            ->assertSuccessful();
        
    }

    public function test_detele_tulisan()
    {
        $tulisan = tulisan::factory()->create();

        Livewire::test(TulisanResource\Pages\EditTulisan::class, [
            'record' => $tulisan->getKey(),
        ])
            ->callPageAction(DeleteAction::class);
        ;

        $this->assertModelMissing($tulisan);
    }
}
