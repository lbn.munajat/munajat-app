<?php

namespace App\Filament\Resources\TulisanResource\Widgets;

use App\Models\tulisan;
use App\Models\User;
use Filament\Widgets\StatsOverviewWidget\Card;
use Filament\Widgets\Widget;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;

class TulisanOverview extends BaseWidget
{
    // protected static string $view = 'filament.resources.tulisan-resource.widgets.tulisan-overview';

    public static string $resource = TulisanResource::class;

    protected function getCards(): array
    {
        return [

            // Card::make('Unique views', '192.1k'),
            // Card::make('Bounce rate', '21%'),
            // Card::make('Average time on page', '3:12'),
            Card::make('Total tulisan', tulisan::count()),
            Card::make('Total User', User::count()),

        ];
    }
}
