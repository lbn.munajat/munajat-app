<?php

namespace Tests\Feature;

use App\Filament\Resources\KategoriResource;
use App\Models\Kategori;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class ListKategoriTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_render_page()
    {
        $this->get(KategoriResource::getUrl('index'))->assertSuccessful();
    }

    public function test_list_kategori()
    {
        $kategoris = Kategori::factory()->count(10)->create();

        Livewire::test(KategoriResource\Pages\ListKategoris::class)
            ->assertCanSeeTableRecords($kategoris)
        ;
    }

    public function test_render_name()
    {
        Livewire::test(KategoriResource\Pages\ListKategoris::class)
        ->assertCanRenderTableColumn('name');
    }

    public function test_render_slug()
    {
        Livewire::test(KategoriResource\Pages\ListKategoris::class)
        ->assertCanRenderTableColumn('slug');
    }

    public function test_render_is_visible()
    {
        Livewire::test(KategoriResource\Pages\ListKategoris::class)
        ->assertCanRenderTableColumn('is_visible');
    }

    public function test_render_is_updated_at()
    {
        Livewire::test(KategoriResource\Pages\ListKategoris::class)
        ->assertCanRenderTableColumn('updated_at');
    }
}
