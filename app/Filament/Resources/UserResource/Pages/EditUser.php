<?php

namespace App\Filament\Resources\UserResource\Pages;
use Filament\Notifications\Notification;
use App\Filament\Resources\UserResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditUser extends EditRecord
{
    protected static string $resource = UserResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }

    protected function afterSave(): void
    {
        $recipient = auth()->user();
 
        Notification::make()
        ->title('Saved successfully')
        ->sendToDatabase($recipient);
  }  


}