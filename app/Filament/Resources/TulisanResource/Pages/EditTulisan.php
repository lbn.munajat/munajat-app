<?php

namespace App\Filament\Resources\TulisanResource\Pages;

use App\Filament\Resources\TulisanResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditTulisan extends EditRecord
{
    protected static string $resource = TulisanResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
