<?php

namespace Database\Seeders;

use App\Models\tulisan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class tulisanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        tulisan::factory()->count(10)->create();
    }
}
