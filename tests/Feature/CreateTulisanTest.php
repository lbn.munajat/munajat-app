<?php

namespace Tests\Feature;

use App\Filament\Resources\TulisanResource;
use App\Models\tulisan;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class CreateTulisanTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_render_page()
    {
        tulisan::factory()->count(10)->create();

        Livewire::test(TulisanResource\Pages\CreateTulisan::class)
            ->assertSuccessful();
        ;
    }

    public function test_create_tulisan()
    {
        $newData = tulisan::factory()->make();

        Livewire::test(TulisanResource\Pages\CreateTulisan::class)
            ->fillForm([

                'title' => $newData->title,
                'content' => $newData->content,
                'is_published' => $newData->is_published,
                'user_id' => $newData->user_id,
                'kategori_id' => $newData->kategori_id,
            ])

            ->call('create')
            ->assertHasNoFormErrors()
        ;

        $this->assertDatabaseHas(tulisan::class, [

            'title' => $newData->title,
            'content' => $newData->content,
            'is_published' => $newData->is_published,
            'user_id' => $newData->user_id,
           'kategori_id' => $newData->kategori_id,
        ]);
    }

    public function test_validate_input()
    {
        $newData = tulisan::factory()->make();

        Livewire::test(TulisanResource\Pages\CreateTulisan::class)
            ->fillForm([
                'title' => null,
            ])
            ->call('create')
            ->assertHasFormErrors(['title' => 'required'])
        ;
    }

}
