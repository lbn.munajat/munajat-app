<?php

namespace Tests\Feature;

use App\Filament\Resources\TulisanResource;
use App\Models\tulisan;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class EditTulisanTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_render_edit()
    {
        $tulisan =tulisan::factory()->create();

        Livewire::test(TulisanResource\Pages\EditTulisan::class, [
        'record' => $tulisan->getKey(),

        ])
            ->assertSuccessful();
        ;
    }
}
