<?php

namespace Tests\Feature;

use App\Filament\Resources\KategoriResource;
use App\Models\Kategori;
use Filament\Pages\Actions\DeleteAction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class DeleteKategoriTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_render_page()
    {
        $kategori = Kategori::factory()->create();
        

        Livewire::test(KategoriResource\Pages\EditKategori::class,
        [
                    'record' => $kategori->getKey(),
        ])
            ->assertSuccessful();
        
    }

    public function test_detele_kategori()
    {
        $kategori = Kategori::factory()->create();

        Livewire::test(KategoriResource\Pages\EditKategori::class, [
            'record' => $kategori->getKey(),
        ])
            ->callPageAction(DeleteAction::class)
        ;

        $this->assertModelMissing($kategori);
    }
}
