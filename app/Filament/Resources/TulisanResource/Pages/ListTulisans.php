<?php

namespace App\Filament\Resources\TulisanResource\Pages;

use App\Filament\Resources\TulisanResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListTulisans extends ListRecords
{
    protected static string $resource = TulisanResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }

    protected function getHeaderWidgets(): array
    {
        return [
            TulisanResource\Widgets\TulisanOverview::class,
        ];
    }
}
