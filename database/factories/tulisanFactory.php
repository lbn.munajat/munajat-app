<?php

namespace Database\Factories;

use App\Models\Kategori;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\tulisan>
 */
class tulisanFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [

            'title' => fake()->sentence(),
            'slug' => fake()->sentence(),
            'content' => 'content',
            'is_published' => false,
            'kategori_id'=> Kategori::factory(),

            'user_id' => User::factory(),

        ];
    }
}
