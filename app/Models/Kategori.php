<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Kategori extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
        'description',
        'is_visible',

    ];

    protected $casts = [
        'is_visible' => 'boolean',
    ];

    public function tulisans(): HasMany
    {
        return $this->hasMany(tulisan::class);
    }
}
