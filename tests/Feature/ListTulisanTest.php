<?php

namespace Tests\Feature;

use App\Filament\Resources\TulisanResource;
use App\Models\tulisan;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class ListTulisanTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     *
     */
    public function test_render_page()
    {
        $this->get(TulisanResource::getUrl('index'))->assertSuccessful();
    }

    public function test_list_tulisan()
    {
        $tulisans = tulisan::factory()->count(10)->create();

        Livewire::test(TulisanResource\Pages\ListTulisans::class)
            ->assertCanSeeTableRecords($tulisans)
        ;
    }

    public function test_render_title()
    {
        Livewire::test(TulisanResource\pages\ListTulisans::class)
        ->assertCanRenderTableColumn('title');
    }

    public function test_render_content()
    {
        Livewire::test(TulisanResource\pages\ListTulisans::class)
        ->assertCanRenderTableColumn('slug');
    }

    public function test_render_image()
    {
        Livewire::test(TulisanResource\pages\ListTulisans::class)
        ->assertCanRenderTableColumn('image');
    }

    public function test_is_published()
    {
        $tulisans = tulisan::factory()->count(10)->create();

        Livewire::test(TulisanResource\Pages\ListTulisans::class)
            ->assertCanSeeTableRecords($tulisans->where('is_published', false))

        ;
    }
}
