<?php

namespace Tests\Feature;

use App\Filament\Resources\KategoriResource;
use App\Models\Kategori;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class EditKategoriTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_render_edit()
    {
        $kategori =Kategori::factory()->create();

        Livewire::test(KategoriResource\Pages\EditKategori::class, [
        'record' => $kategori->getKey(),

        ])
            ->assertSuccessful();
        ;
    }
}
