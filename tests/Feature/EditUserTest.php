<?php

namespace Tests\Feature;

use App\Filament\Resources\UserResource;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class EditUserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_render_edit()
    {
        $user =User::factory()->create();

        Livewire::test(UserResource\Pages\EditUser::class, [
        'record' => $user->getKey(),

        ])
            ->assertSuccessful();
        ;
    }
}
