<?php

namespace Tests\Feature;

use App\Filament\Resources\KategoriResource;
use App\Models\Kategori;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class CreateKategoriTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_render_page()
    {
        Kategori::factory()->count(10)->create();

        Livewire::test(KategoriResource\Pages\CreateKategori::class)
            ->assertSuccessful();
        ;
    }

    public function test_create_kategori()
    {
        $newData = Kategori::factory()->make();

        Livewire::test(KategoriResource\Pages\CreateKategori::class)
            ->fillForm([

                'name' => $newData->name,
                'slug' => $newData->slug,
                'description' => $newData->description,
                'is_visible' => $newData->is_visible,

            ])

            ->call('create')
            ->assertHasNoFormErrors()
        ;

        $this->assertDatabaseHas(Kategori::class, [

            'name' => $newData->name,
            'slug' => $newData->slug,
            'description' => $newData->description,
            'is_visible' => $newData->is_visible,
        ]);
    }
}
