<?php

namespace App\Providers;

use App\Filament\Resources\KategoriResource;
use App\Filament\Resources\TulisanResource;
use App\Filament\Resources\UserResource;
use App\Models\tulisan;
use Filament\Facades\Filament;
use Filament\Navigation\NavigationBuilder;
use Filament\Navigation\NavigationGroup;
use Filament\Navigation\NavigationItem;
use Filament\Navigation\UserMenuItem;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\HtmlString;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot(): void
    {
        filament::serving(function () {
            Filament::registerNavigationGroups([
                NavigationGroup::make()
                    ->label('Blog')
                    ->icon('heroicon-s-cog')
                    ->collapsed(),
            ]);
        });


        // Filament::serving(function () {
        //     Filament::registerNavigationGroups([
        //         'Tulisan',
        //         'Kategori',
        //         'User',
        //     ]);
        // });

        // Filament::serving(function () {
        //     Filament::registerNavigationItems([
        //         NavigationItem::make('Analytics')
        //             ->url('https://filament.pirsch.io', shouldOpenInNewTab: true)
        //             ->icon('heroicon-o-presentation-chart-line')
        //             ->activeIcon('heroicon-s-presentation-chart-line')
        //             ->group('Reports')
        //             ->sort(3),
        //     ]);
        // });

        // Filament::navigation(function (NavigationBuilder $builder): NavigationBuilder {
        //     return $builder;
        // });

        // Filament::navigation(function (NavigationBuilder $builder): NavigationBuilder {
        //     return $builder->items([
        //         NavigationItem::make('Dashboard')
        //             ->icon('heroicon-o-home')
        //             ->activeIcon('heroicon-s-home')
        //             ->isActiveWhen(fn (): bool => request()->routeIs('filament.pages.dashboard'))
        //             ->url(route('filament.pages.dashboard')),
        //         ...UserResource::getNavigationItems(),

        //     ]);
        // });


        // Filament::navigation(function (NavigationBuilder $builder): NavigationBuilder {
        //     return $builder
        //         ->groups([
        //             NavigationGroup::make('Website')
        //                 ->items([
        //                     ...KategoriResource::getNavigationItems(),
        //                     ...TulisanResource::getNavigationItems(),
        //                     ...UserResource::getNavigationItems(),
                        
        //                 ]),
        //         ]);
        // });

        Filament::serving(function () {
            Filament::registerUserMenuItems([
                UserMenuItem::make()
                    ->label('Kategori')
                    ->url(route('filament.resources.kategoris.index'))
                    ->icon('heroicon-s-cog'),
                // ...
            ]);
        });

        Filament::serving(function (){
            Filament::registerUserMenuItems([
                'User' => UserMenuItem::make()->url(route('filament.resources.users.index'))
                ->label('Add Users'),
            ]);
        });

        Filament::serving(function () {
            Filament::registerUserMenuItems([
                // ...
                'logout' => UserMenuItem::make()->label('Log out guys '),
            ]);
        });


        Filament::serving(function () {
            // Using Vite
            Filament::registerViteTheme('resources/css/filament.css');
         
        });


        Filament::registerScripts([
            asset('js/my-script.js'),
        ]);
         
        Filament::registerStyles([
            'https://unpkg.com/tippy.js@6/dist/tippy.css',
            asset('css/my-styles.css'),
        ]);

        Filament::registerScripts([
            'https://cdn.jsdelivr.net/npm/@ryangjchandler/alpine-tooltip@0.x.x/dist/cdn.min.js',
        ], true);

        Filament::pushMeta([
            new HtmlString('<link rel="manifest" href="/site.webmanifest" />'),
        ]);

        Filament::registerRenderHook(
            'sidebar.end',
            fn (): string => Blade::render('hello word'),
        );

       
        
    }
}
