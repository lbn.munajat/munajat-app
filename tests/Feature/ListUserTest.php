<?php

namespace Tests\Feature;

use App\Filament\Resources\UserResource;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class ListUserTest extends TestCase
{
     use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
   public function test_render_page()
    {
        $this->get(UserResource::getUrl('index'))->assertSuccessful();
    }

    public function test_render_name()
    {
        Livewire::test(UserResource\pages\ListUsers::class)
        ->assertCanRenderTableColumn('name');
    }

    public function test_render_email()
    {
        Livewire::test(UserResource\pages\ListUsers::class)
        ->assertCanRenderTableColumn('email');
    }

    public function test_render_github_username()
    {
        Livewire::test(UserResource\pages\ListUsers::class)
        ->assertCanRenderTableColumn('github_username');
    }


    public function test_render_twitter_username()
    {
        Livewire::test(UserResource\pages\ListUsers::class)
        ->assertCanRenderTableColumn('twitter_username');
    }




    

}
