<?php

namespace Tests\Feature;

use App\Filament\Resources\KategoriResource;
use App\Filament\Resources\UserResource;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    
     public function test_render_page()
     {
         User::factory()->count(10)->create();
 
         Livewire::test(UserResource\Pages\CreateUser::class)
             ->assertSuccessful();
         
     }

     public function test_create_tulisan()
    {
        $newData =  User::factory()->make();

        Livewire::test(UserResource\Pages\CreateUser::class)
            ->fillForm([

                'name' => $newData->name,
                'email' => $newData->email,
                'bio' => $newData->bio,
                'github_username' => $newData->github_username,
                'twitter_username' => $newData->twitter_username,
            ])

            ->call('create')
            ->assertHasNoFormErrors()
        ;

        $this->assertDatabaseHas(User::class, [

            'name' => $newData->name,
            'email' => $newData->email,
            'github_username' => $newData->github_username,
           'twitter_username' => $newData->twitter_username,
        ]);
    }

    public function test_validate_input()
    {
        $newData = User::factory()->make();

        Livewire::test(UserResource\Pages\CreateUser::class)
            ->fillForm([
                'name' => null,
            ])
            ->call('create')
            ->assertHasFormErrors(['name' => 'required'])
        ;
    }
}
