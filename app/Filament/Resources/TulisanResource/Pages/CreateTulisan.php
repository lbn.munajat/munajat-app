<?php

namespace App\Filament\Resources\TulisanResource\Pages;

use App\Filament\Resources\TulisanResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateTulisan extends CreateRecord
{
    protected static string $resource = TulisanResource::class;

    protected function mutateFormDataBeforeCreate(array $data): array
    {
        $data['user_id'] = auth()->id();

        return $data;
    }
}
